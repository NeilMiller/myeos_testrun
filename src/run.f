program run
  use run_star_support, only: do_read_star_job
  use run_star, only: do_run_star
  
  implicit none
  
  integer :: ierr

  write(*,*) "[RUN] Calling do_read_star_job"
  ierr = 0
  call do_read_star_job('inlist', ierr)
  if (ierr /= 0) stop 1
  
  write(*,*) "[RUN] Calling do_run_star"
  call do_run_star
  
end program run
